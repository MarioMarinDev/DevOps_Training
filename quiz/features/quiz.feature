Feature: Answer the quiz
    In order to feed our stats related to the python survey
    As cigna members
    We need to answer the quiz

    Scenario: User provides bad lanid
        Given I go to the "http://127.0.0.1:8080/" page
        When I insert the lanid "diu344" and try to go to the next screen
        Then the error message "Enter a valid value." is shown

    Scenario: Redirection to a quiz with a valid lanid
        Given I go to the "http://127.0.0.1:8080/" page
        When I insert the lanid "M25101" and try to go to the next screen
        Then the current url should be "http://127.0.0.1:8080/do-quiz/M25101/"

    Scenario: Check if lanid is shown on the quiz
        Given I go to the "http://127.0.0.1:8080/" page
        When I insert the lanid "M25101" and try to go to the next screen
        Then the quiz should contain "M25101" in the title

    Scenario: Check if there's a question in que quiz
        Given I go to the "http://127.0.0.1:8080/" page
        When I insert the lanid "M25101" and try to go to the next screen
        Then the quiz should contain a question