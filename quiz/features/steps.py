# -*- coding: utf-8 -*-

import time
from lettuce import step, world, before
from selenium import webdriver


@before.all
def prepare_tests():
    world.browser = webdriver.Chrome()


@step('Given I go to the "(.*)" page')
def given_i_go_to_the_group1_page(step, page):
    time.sleep(3)
    world.url = page
    world.browser.get(world.url)


@step('When I insert the lanid "(.*)" and try to go to the next screen')
def when_i_insert_the_lanid_group1_and_try_to_go_to_the_next_screen(step, lanid):
    time.sleep(5)
    lanid_input = world.browser.find_element_by_id('id_lanid')
    lanid_input.send_keys(lanid)
    time.sleep(2)
    xpath = '//button[contains(text(), "Next")]'
    next_button = world.browser.find_element_by_xpath(xpath)
    next_button.click()


@step('Then the error message "(.*)" is shown')
def then_the_error_message_group1_is_shown(step, expected_error):
    time.sleep(3)

    xpath = '//div[@class="red-text"]'
    error_message = world.browser.find_element_by_xpath(xpath)

    assert expected_error == error_message.text, "Got: %s == %s" % error_message.text % expected_error


@step('the current url should be "(.*)"')
def the_current_url_should_be_group1(step, expected_url):
    time.sleep(3)
    chrome = world.browser
    url = chrome.current_url
    assert url == expected_url, "Got: %s" % url


@step('the quiz should contain "(.*)" in the title')
def the_quiz_should_contain_group1_in_the_title(step, expected_lanid):
    time.sleep(3)
    xpath = '//span[contains(text(), "Python Survey")]'
    actual_title = world.browser.find_element_by_xpath(xpath)
    assert expected_lanid in actual_title.text, "Got: %s" % actual_title


@step('the quiz should contain a question')
def the_quiz_should_contain_a_question(step):
    time.sleep(3)
    xpath = '//div[@class="input-field"]'
    assert world.browser.find_element_by_xpath(xpath), "Got nothing"
