## Quizzer

Project for quiz generation.

## Getting Started

Fork the repository and run the project locally.

```
$ cd quizzer
$ pip install -r requirements.txt
$ python manage.py runserver

```

## Built With

* [Django](https://www.djangoproject.com/)- Django Web Framework 

# Docker notes
```
$ docker build -t "new_image_name" "Dockerfile location"
$ docker run -d -p 8080:8080 -P --name="quiz" "new_image_name"
```

# Test (Centos7)
## Install links
```
$ sudo yum install links
```
## Use links
```
$ links 127.0.0.1:8080
```