FROM python:2

ADD / /home/user/DevOps_Django/

RUN pip install --upgrade pip

RUN pip install -r /home/user/DevOps_Django/requirements.txt

EXPOSE 8080

CMD ["python", "/home/user/DevOps_Django/manage.py", "runserver", "0.0.0.0:8080"]